import UseContextHook from "./Hooks/UseContext/useContext";
import UseEffectHook from "./Hooks/UseEffect/useEffect";
import UseStateHook from "./Hooks/UseState/useState";
import './App.css'
import UseReducerHook from "./Hooks/UseReducer/useReducer";
import UseRefHook from "./Hooks/UseRef/useRef";
import UseLayoutHook from "./Hooks/UseLayout/useLayout";
import ImperativeHandleHook from "./Hooks/UseImperativeHandle/useImperativeHandle";
import UseMemoHook from "./Hooks/UseMemo/useMemo";
import UseCallBackHook from "./Hooks/UseCallback/useCallback";
const App = () => {
  return (
    <div > 
      <div className="hook-container">
        <UseStateHook />
      </div>
      <div className="hook-container">
        <UseEffectHook />
      </div>
      <div className="hook-container">
        <UseContextHook />
      </div>
      <div className="hook-container">
        <UseReducerHook/> 
      </div>
      <div className="hook-container">
        <UseRefHook/>
      </div>
      <div className="hook-container">
        <UseLayoutHook/>
      </div>
      <div className="hook-container">
        <ImperativeHandleHook/>
      </div>
      <div className="hook-container">
        <UseMemoHook/>
      </div>
      <div className="hook-container">
        <UseCallBackHook/>
      </div>
    </div>
  );
};

export default App;
