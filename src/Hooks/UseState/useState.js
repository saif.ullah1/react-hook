import React, { useState } from "react";

const UseStateHook = () => {
  const [inputValue, setInputValue] = useState("Typing");

  let onChange = (event) => {
    const newValue = event.target.value;
    setInputValue(newValue);
  };

  return (
    <div>
      <h2>UseState Hook</h2>
      <input placeholder="enter something..." onChange={onChange} />
      {inputValue}
    </div>
  );
};

export default UseStateHook;