import React, { useRef } from "react";
import Button from "./Button";

function ImperativeHandleHook() {
  const buttonRef = useRef(null);
  return (
    <div>
      <h2>useImperativeHandle Hook</h2>
      <button
        onClick={() => {
          buttonRef.current.alterToggle();
        }}
      >
        Button From Parent
      </button>
      <Button ref={buttonRef} />
    </div>
  );
}

export default ImperativeHandleHook;