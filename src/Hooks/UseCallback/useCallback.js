import axios from "axios";
import { useCallback, useState } from "react";
import Child from "./Child";

 const UseCallBackHook=()=> {
  const [toggle, setToggle] = useState(false);
  const [data, setData] = useState("Learning Hooks");

  const returnComment = useCallback(
    (name) => {
      return data + name;
    },
    [data]
  );

  return (
    <div className="App">
      <h2>UseCallback Hook</h2>
      <Child returnComment={returnComment} />

      <button
        onClick={() => {
          setToggle(!toggle);
        }}
      >
        {" "}
        Toggle
      </button>
      {toggle && <h1> toggle </h1>}
    </div>
  );
}
export default UseCallBackHook;