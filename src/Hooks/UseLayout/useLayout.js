import { useLayoutEffect, useEffect, useRef } from "react";

const UseLayoutHook=()=> {
  const inputRef = useRef(null);

  useLayoutEffect(() => {
    console.log(inputRef.current.value);
  }, []);

  useEffect(() => {
    inputRef.current.value = "HELLO";
  }, []);

  return (
    <div className="App">
      <h2>UseLayout Hook</h2>
      <input ref={inputRef} value="Saif" style={{ width: 400, height: 60 }} />
    </div>
  );
}

export default UseLayoutHook;